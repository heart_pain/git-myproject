package com.chenguo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by Donghua.Chen on 2018/6/20.
 */
//@Api("注释")
@ApiModel("用户实体")
public class User implements Serializable {
    private static final long serialVersionUID = 8655851615465363473L;

    //@ApiModel为类添加注释
    //@ApiModelProperty为类属性添加注释

    //@Api(tags = “xxx模块说明”)	作用在模块类上
    //@ApiOperation(“xxx接口说明”)	作用在接口方法上
    //@ApiModel(“xxxPOJO说明”)	作用在模型类上：如VO、BO
    //@ApiModelProperty(value = “xxx属性说明”,hidden = true)	作用在类方法和属性上，hidden设置为true可以隐藏该属性
    //@ApiParam(“xxx参数说明”)	作用在参数、方法和字段上，类似@ApiModelProperty

    private Long id;
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("密码")
    private String password;

    public User() {
    }

    public User(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
