package com.chenguo.controller;

import com.chenguo.pojo.User;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * 是一个规范和完整的框架，用于生成、描述、调用和可视化 RESTful 风格的 Web 服务
 * Created by Chen on 2018/6/20.
 */
@RestController
@Api(tags = "1.1", value = "用户管理")
@RequestMapping("/user")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);


    //   /error默认错误请求
    @ApiOperation("Hello控制接口")
    @GetMapping("/hello")
    public String hello(@ApiParam("用户名") String username) {
        return "hello"+username;
    }

    //只要我们的接口中，返回值中存在实体类，他就会被扫描到Swagger中
    @PostMapping("/get")
    @ApiOperation("get测试")
    public User getuser(@ApiParam("用户名") User user) {
        return new User();
    }
    //@Api(tags = “xxx模块说明”)	作用在模块类上
    //@ApiOperation(“xxx接口说明”)	作用在接口方法上
    //@ApiModel(“xxxPOJO说明”)	作用在模型类上：如VO、BO
    //@ApiModelProperty(value = “xxx属性说明”,hidden = true)	作用在类方法和属性上，hidden设置为true可以隐藏该属性
    //@ApiParam(“xxx参数说明”)	作用在参数、方法和字段上，类似@ApiModelProperty


  /*  @GetMapping("/users")
    @ApiOperation(value = "条件查询（DONE）")//接口名字
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名",//参数名称 说明
                   //类型
                    dataType = ApiDataType.STRING,
                    //IN
                    paramType = ApiParamType.QUERY),
            @ApiImplicitParam(name = "password", value = "密码",
                    dataType = ApiDataType.STRING, paramType = ApiParamType.QUERY),
    })
    public User query(String username, String password) {
        log.info("多个参数用  @ApiImplicitParams");
        return new User(1L, username, password);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "主键查询（DONE）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户编号", dataType = ApiDataType.LONG, paramType = ApiParamType.PATH),
    })
    public User get(@PathVariable Long id) {
        log.info("单个参数用  @ApiImplicitParam");
        return new User(id, "u1", "p1");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除用户（DONE）")
    @ApiImplicitParam(name = "id", value = "用户编号", dataType = ApiDataType.LONG, paramType = ApiParamType.PATH)
    public void delete(@PathVariable Long id) {
        log.info("单个参数用 ApiImplicitParam");
    }

    @PostMapping
    @ApiOperation(value = "添加用户（DONE）")
    public User post(@RequestBody User user) {
        log.info("如果是 POST PUT 这种带 @RequestBody 的可以不用写 @ApiImplicitParam");
        return user;
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "修改用户（DONE）")
    public void put(@PathVariable Long id, @RequestBody User user) {
        log.info("如果你不想写 @ApiImplicitParam 那么 swagger 也会使用默认的参数名作为描述信息 ");
    }*/
}
