package com.chenguo.controller;/**
 * @Author chengguo
 * @ClassName AsyncController
 * @Date 2021-07-16  14:25
 * @Version 1.0
 **/

import com.chenguo.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author chengguo
 * @ClassName AsyncController
 * @Date 2021-07-16  14:25
 * @Version 1.0
 **/
@RestController
public class AsyncController {

    @Autowired
    AsyncService asyncService;

    @GetMapping("/hello")
    public String hello() {
        asyncService.hello();
        return "OK";
    }
}
