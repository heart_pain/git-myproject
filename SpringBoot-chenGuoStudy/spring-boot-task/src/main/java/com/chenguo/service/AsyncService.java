package com.chenguo.service;/**
 * @Author chengguo
 * @ClassName AsyncService
 * @Date 2021-07-16  14:26
 * @Version 1.0
 **/

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @Author chengguo
 * @ClassName AsyncService
 * @Date 2021-07-16  14:26
 * @Version 1.0
 **/
@Service
public class AsyncService {

    @Async//告诉spring这是一个异步方法
    public void hello(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("业务进行中....");
    }
}
