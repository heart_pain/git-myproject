package com.chenguo.task;

import com.alibaba.fastjson.JSON;
import com.chenguo.pojo.DemoEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 定时任务
 * 1. 在启动类加上@EnableScheduling注解
 *
 * 2. 在controller的类上加上@Component注解
 *
 * 3. 在controller的方法上加上@Scheduled注解即可
 * Created by Donghua.Chen on 2018/7/24.
 */
@Component
public class SpringTaskDemo {

    private static final Logger log = LoggerFactory.getLogger(SpringTaskDemo.class);

    @Async
    @Scheduled(cron = "0/1 * * * * *")
                    // 秒  分 时 月 周 年
    public void scheduled1() throws InterruptedException {
        Thread.sleep(30000);
        DemoEntity demoEntity = new DemoEntity("chenguo","湖北",new Date());
        log.info("scheduled1 每1秒执行一次：{}", LocalDateTime.now());
        log.info("这是一条json数据:", JSON.toJSONString(demoEntity));  //可以放在代码里  也可以放在catch里面
    }

    //每隔1秒执行一次任务
    @Scheduled(fixedRate = 1000)
    public void scheduled2() throws InterruptedException {
        Thread.sleep(30000);
        log.info("scheduled2 每3秒执行一次：{}", LocalDateTime.now());
    }


    /*只是说是 fixedRate 任务两次执行时间间隔是任务的开始点，而 fixedDelay 的间隔是前次任务的结束与下次任务的开始*/
    /*以前我对 fixedRate 还有一个误区就是，以为任务时长超过 fixedRate 时会启动多个任务实例，
    其实不会; 只不过会在上次任务执行完后立即启动下一轮。除非这个 Job 方法用 @Async 注解了，使得任务不在 TaskScheduler 线程池中执行，而是每次创建新线程来执行*/
    @Scheduled(fixedDelay = 3000)
    public void scheduled3() throws InterruptedException {
        Thread.sleep(50000);
        log.info("scheduled3 上次执行完毕后隔3秒继续执行：{}", LocalDateTime.now());
    }
}
