> springboot系列教程，有原创和转载，希望能帮到大家。

**springboot2.X系列：**

*  [springboot整合mybatis 使用HikariCP连接池](https://blog.csdn.net/Winter_chen001/article/details/81204116)
* [springboot集成hadoop实战](https://winterchen.blog.csdn.net/article/details/110449133)
* [springboot集成hive实战](https://winterchen.blog.csdn.net/article/details/110449266)
* [springboot集成Oozie实战](https://winterchen.blog.csdn.net/article/details/110449314)

**1、Actuator：springboot程序监控器** 
**2、自动装配：简单配置甚至零配置即可运行项目**
**3、starter：jar包的引入，解决jar版本冲突问题**
**4、CLI：命令行**

* [一起来学SpringBoot | 第一篇：构建第一个SpringBoot工程](https://blog.csdn.net/winter_chen001/article/details/80537847)
* [一起来学SpringBoot | 第二篇：SpringBoot配置详解](https://blog.csdn.net/winter_chen001/article/details/80537874) 
* [一起来学SpringBoot | 第三篇：SpringBoot日志配置](https://blog.csdn.net/winter_chen001/article/details/80537897)
```spring-boot-start 快速启动```
```spring-boot-task 定时任务  加一个Logger```

```spring-boot-Thymeleaf```
* [一起来学SpringBoot | 第四篇：整合Thymeleaf模板](https://blog.csdn.net/winter_chen001/article/details/80537918)
```spring-boot-jdbctemplate    user表  id username password```
* [一起来学SpringBoot | 第五篇：使用JdbcTemplate访问数据库](https://blog.csdn.net/winter_chen001/article/details/80537933)
```spring-boot-data-jpa-demo   user表  id username password```
* [一起来学SpringBoot | 第六篇：整合SpringDataJpa](https://blog.csdn.net/winter_chen001/article/details/80537952)
```spring-boot-data-mybatis   user表  id username password```
* [一起来学SpringBoot | 第七篇：整合Mybatis](https://blog.csdn.net/Winter_chen001/article/details/80614264)
* [一起来学SpringBoot | 第八篇：通用Mapper与分页插件的集成](https://blog.csdn.net/winter_chen001/article/details/80614322)
```spring-boot-lettuce-redis ```
* [一起来学SpringBoot | 第九篇：整合Lettuce Redis](https://blog.csdn.net/winter_chen001/article/details/80614331)
```spring-boot-cache-redis @CachePut  存、 @Cacheable 取、@CacheEvict  销毁```
* [一起来学SpringBoot | 第十篇：使用Spring Cache集成Redis](https://blog.csdn.net/Winter_chen001/article/details/80731529)
```spring-boot-Swagger 是一个规范和完整的框架，用于生成、描述、调用和可视化 RESTful 风格的 Web 服务```
* [一起来学SpringBoot | 第十一篇：集成Swagger在线调试](https://blog.csdn.net/Winter_chen001/article/details/80748253)
```spring-boot-rabbitmq ```
* [一起来学SpringBoot | 第十二篇：初探RabbitMQ消息队列](https://blog.csdn.net/Winter_chen001/article/details/80750151)
* [一起来学SpringBoot | 第十三篇：RabbitMQ延迟队列](https://blog.csdn.net/Winter_chen001/article/details/81011847)

```spring-boot-starter-actuator```
* [一起来学SpringBoot | 第十四篇：强大的 actuator 服务监控与管理](https://blog.csdn.net/Winter_chen001/article/details/81014004)
```spring-boot-starter-actuator```
* [一起来学SpringBoot | 第十五篇：actuator与spring-boot-admin 可以说的秘密 ](https://blog.csdn.net/Winter_chen001/article/details/81018644)
```spring-boot-starter-test```
* [一起来学SpringBoot | 第十六篇：定时任务详解](https://blog.csdn.net/Winter_chen001/article/details/81180086)

```spring-boot-file-upload```
* [一起来学SpringBoot | 第十七篇：轻松搞定文件上传](https://blog.csdn.net/Winter_chen001/article/details/81180941)
```spring-boot-exception```
* [一起来学SpringBoot | 第十八篇：轻松搞定全局异常](https://blog.csdn.net/Winter_chen001/article/details/81181339)
```spring-boot-validation1 后端验证数据```
* [一起来学SpringBoot | 第十九篇：轻松搞定数据验证（一）](https://blog.csdn.net/Winter_chen001/article/details/81196720)
* [一起来学SpringBoot | 第二十篇：轻松搞定数据验证（二）](https://blog.battcn.com/2018/06/06/springboot/v2-other-validate2/)
* [一起来学SpringBoot | 第二十一篇：轻松搞定数据验证（三） ](https://blog.battcn.com/2018/06/07/springboot/v2-other-validate3/)
```spring-boot-chapter  后端验证数据```
* [一起来学SpringBoot | 第二十二篇：轻松搞定重复提交（本地锁）](https://blog.battcn.com/2018/06/12/springboot/v2-cache-locallock/)

```spring-boot-chapterplus  没看懂```
* [一起来学SpringBoot | 第二十三篇：轻松搞定重复提交（分布式锁）](https://blog.battcn.com/2018/06/13/springboot/v2-cache-redislock/)
```spring-boot-liquibase```
* [一起来学SpringBoot | 第二十四篇：数据库管理与迁移（Liquibase）](https://blog.battcn.com/2018/06/20/springboot/v2-other-liquibase/)
* [一起来学SpringBoot | 第二十五篇：打造属于你的聊天室（WebSocket）](https://blog.battcn.com/2018/06/27/springboot/v2-other-websocket/)
* [一起来学SpringBoot | 第二十六篇：轻松搞定安全框架（Shiro）](https://blog.battcn.com/2018/07/03/springboot/v2-other-shiro/)
* [一起来学SpringBoot | 第二十七篇：优雅解决分布式限流](https://blog.battcn.com/2018/08/08/springboot/v2-cache-redislimter/)
* [一起来学SpringBoot | 第二十八篇：JDK8 日期格式化](https://blog.battcn.com/2018/10/01/springboot/v2-localdatetime/)
* [[SpringBoot2.X] springboot Mybatis 整合](https://blog.csdn.net/winter_chen001/article/details/80010967)
* [[SpringBoot2.X] springboot mybatis 使用多数据源](https://blog.csdn.net/winter_chen001/article/details/80513993)
* 持续更新中...

**springboot1.5.X系列：**

* [Spring Boot快速入门](http://blog.csdn.net/winter_chen001/article/details/78330002)
* [Spring Boot开发Web应用](http://blog.csdn.net/winter_chen001/article/details/78330142)
* [Spring Boot工程结构推荐](http://blog.csdn.net/winter_chen001/article/details/78330181)
* [Spring Boot构建RESTful API与单元测试](http://blog.csdn.net/winter_chen001/article/details/78330253)
* [Spring Boot中使用Swagger2构建强大的RESTful API文档](http://blog.csdn.net/winter_chen001/article/details/78330687)
* [Spring Boot中使用JdbcTemplate访问数据库](http://blog.csdn.net/winter_chen001/article/details/78508328)
* [Spring Boot中使用Spring-data-jpa让数据访问更简单、更优雅](http://blog.csdn.net/winter_chen001/article/details/78508337)
* [Spring Boot多数据源配置与使用](http://blog.csdn.net/winter_chen001/article/details/78508376)
* [Spring Boot日志管理](http://blog.csdn.net/winter_chen001/article/details/78508381)
* [Spring Boot中使用Redis数据库](http://blog.csdn.net/winter_chen001/article/details/78508393)
* [Spring Boot中使用MongoDB数据库](http://blog.csdn.net/winter_chen001/article/details/78508398)
* [Spring Boot中Web应用的统一异常处理](http://blog.csdn.net/winter_chen001/article/details/78508408)
* [Spring Boot属性配置文件详解](http://blog.csdn.net/winter_chen001/article/details/78508415)
* [Spring Boot中使用@Scheduled创建定时任务](http://blog.csdn.net/winter_chen001/article/details/78508421)
* [Spring Boot中使用@Async实现异步调用](http://blog.csdn.net/winter_chen001/article/details/78508429)
* [Spring boot Mybatis 整合（完整版）](https://blog.csdn.net/winter_chen001/article/details/77249029)
* [Spring boot Mybatis 整合（注解版）](http://blog.csdn.net/winter_chen001/article/details/78622141)
* [springboot事务管理详解](http://blog.csdn.net/winter_chen001/article/details/78622679)
* [springboot中使用Mybatis注解配置详解](http://blog.csdn.net/winter_chen001/article/details/78623700)

* [spring中添加自定义的拦截器](https://blog.csdn.net/winter_chen001/article/details/80237100)





springboot技术交流群：681513531

个人博客：https://blog.winterchen.com

全文代码：https://github.com/WinterChenS/springboot-learning-experience






































