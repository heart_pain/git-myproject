package com.chenguo.fanout;


import com.chenguo.rabbit.fanout.FanoutSender;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;




/**
 * Created by Donghua.Chen on 2018/4/27.
 */

@SpringBootTest
public class FanoutSenderTest {

    @Autowired
    private FanoutSender fanoutSender;

    @Test
    public void send() throws Exception {

            fanoutSender.send();
    }

}