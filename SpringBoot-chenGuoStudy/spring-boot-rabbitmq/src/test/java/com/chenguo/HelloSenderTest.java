package com.chenguo;


import com.chenguo.pojo.User;
import com.chenguo.rabbit.hello.HelloSender;
import com.chenguo.rabbit.many.NeoSender;
import com.chenguo.rabbit.many.NeoSender2;
import com.chenguo.rabbit.object.ObjectSender;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Donghua.Chen on 2018/4/27.
 */
@SpringBootTest
public class HelloSenderTest {

    @Autowired
    private HelloSender helloSender;

    @Autowired
    private NeoSender neoSender;

    @Autowired
    private NeoSender2 neoSender2;

    @Autowired
    private ObjectSender objectSender;

    @Test
    public void hello() throws Exception {
        helloSender.send();
    }

    @Test
    public void oneToMany(){
        for (int i = 0; i < 100; i++) {
            neoSender.neoSend(i);
        }
    }

    @Test
    public void manyToMany(){
        for (int i = 0; i < 100; i++) {
            neoSender.neoSend(i);
            neoSender2.neoSender(i);
        }
    }

    @Test
    public void objectSend(){
        User user = new User();
        user.setUserName("新垣结衣");
        user.setAge(18);
        objectSender.sendObject(user);
    }

}