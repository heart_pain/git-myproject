package com.chenguo.service;

import com.chenguo.pojo.User;

public interface UserService {

    public User queryUserByName(String name);

}
