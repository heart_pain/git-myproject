package com.chenguo.service;

import com.chenguo.dao.UserMapper;
import com.chenguo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserMapper mapper;

    public User queryUserByName(String name) {
        User user = mapper.queryUserByName(name);
        return user;
    }
}
