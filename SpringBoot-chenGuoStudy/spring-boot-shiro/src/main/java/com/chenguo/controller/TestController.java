package com.chenguo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

@Controller
public class TestController {



        @RequestMapping("/t1")
        public String test1(Model model){
            model.addAttribute("msg","Hello,Thymeleaf");
            //classpath:/templates/test.html
            return "test";
        }

        @RequestMapping("/t2")
        public String test2(Map<String,Object> map){
           map.put("msg","<h1>Hello</h1>");
           map.put("users", Arrays.asList("chenguo","zhuxi"));
            //classpath:/templates/test.html
            return "test";
        }



}
