package com.chenguo;

import com.chenguo.pojo.User;
import com.chenguo.service.UserService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootCacheRedisApplicationTests {


    private static final Logger log = LoggerFactory.getLogger(SpringBootCacheRedisApplicationTests.class);


    @Autowired
    private UserService userService;

     //进入 saveOrUpdate 方法
     //[saveOrUpdate] - [User(id=5, username=u5, password=p5)]
     //[get] - [User(id=5, username=u5, password=p5)]
     //进入 delete 方法
     //查询是没有日志输出的，因为它直接从缓存中获取的数据，
     // 而添加、修改、删除都是会进入方法内执行具体的业务代码，然后通过切面去删除掉Redis中的缓存数据


    @Test
    public void get() {
        final User user = userService.saveOrUpdate(new User(5L, "u5", "p5"));
        log.info("[saveOrUpdate] - [{}]", user);
        final User user1 = userService.get(5L);
        log.info("[get] - [{}]", user1);
        userService.delete(5L);
    }
}
