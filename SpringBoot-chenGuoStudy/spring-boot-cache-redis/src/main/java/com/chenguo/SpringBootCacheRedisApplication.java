package com.chenguo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching //基于注释（annotation）的缓存（cache）技术
public class SpringBootCacheRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCacheRedisApplication.class, args);
    }

}
