package com.chenguo.springbootlettucejedis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootLettuceJedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootLettuceJedisApplication.class, args);
    }

}
