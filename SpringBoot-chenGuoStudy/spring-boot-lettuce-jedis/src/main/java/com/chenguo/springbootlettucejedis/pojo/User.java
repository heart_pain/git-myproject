package com.chenguo.springbootlettucejedis.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Donghua.Chen on 2018/6/7.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable{
    private static final long serialVersionUID = 8655851615465363473L;
    private int id;
    private String username;
    private String password;



}
