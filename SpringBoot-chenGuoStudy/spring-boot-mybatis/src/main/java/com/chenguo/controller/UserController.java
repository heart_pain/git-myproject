package com.chenguo.controller;

import com.chenguo.pojo.UserDomain;
import com.chenguo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/test")
    public String test(){
        return "你好!!!!!";
    }


    @PostMapping("/add")
    public int addUser(UserDomain user){
        return userService.addUser(user);
    }


    @GetMapping("/all")
    public Object findAllUser(
            @RequestParam(name = "pageNum", required = false, defaultValue = "1")
                    int pageNum,
            @RequestParam(name = "pageSize", required = false, defaultValue = "5")
                    int pageSize){
        return userService.findAllUser(pageNum,pageSize);
    }
    @RequestMapping("/list")
    public Object findUsers(){
        System.out.println(userService.findUsers());
        return userService.findUsers();
    }
}
