package com.chenguo.dao;

import com.chenguo.pojo.UserDomain;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface UserMapper {

    int insert(UserDomain record);



    List<UserDomain> selectUsers();


    List<UserDomain> findUsers();

}


