package com.chenguo.springbootchapterplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootChapterplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootChapterplusApplication.class, args);
    }

}
