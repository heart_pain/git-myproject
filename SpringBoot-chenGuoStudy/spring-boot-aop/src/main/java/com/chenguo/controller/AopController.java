package com.chenguo.controller;/**
 * @Author chengguo
 * @ClassName AopController
 * @Date 2021-07-16  16:01
 * @Version 1.0
 **/

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author chengguo
 * @ClassName AopController
 * @Date 2021-07-16  16:01
 * @Version 1.0
 **/
@RestController
@RequestMapping("/aop")
public class AopController {

    @RequestMapping(value = "/sayhello" ,method = RequestMethod.GET)
    public String sayHello(String name){
        return "hello"+name;
    }

   @RequestMapping(value = "/sayhello/{name}" ,method = RequestMethod.GET)
    public String sayHello1(@PathVariable("name") String name){
        return "hello"+name;
    }

}
