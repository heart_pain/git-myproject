##笔记
> 1、手敲了JSR303校验spring-boot-validation    
> 2、模板引擎 spring-boot-thymeleaf(页面国际化就不敲了)
>    还有一个spring-boot-start快速入门  
> 3、整合jdbc  spring-boot-jdbcTemplate  
> 4、整合mybatis  spring-boot-mybatis  
> 5、整合swagger2 spring-boot-swagger  
> 6、整合task  异步、定时任务 spring-boot-task  
> 7、对应用程序进行监视和管理  spring-boot-starter-actuator
> 8、认证授权 spring-boot-shiro   spring-boot-springsecurity
> 9、消息中间件  spring-boot-rabbitmq
> 10、整合mybatis spring-boot-mybatis
> 11、数据库管理和迁移  spring-boot-liquibase
> 12、Jedis 整合 spring-boot-lettuce-jedis




