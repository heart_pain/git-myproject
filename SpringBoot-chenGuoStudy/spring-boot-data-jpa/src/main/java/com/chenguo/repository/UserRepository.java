package com.chenguo.repository;

import com.chenguo.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
        //1. 一个使用@Query注解的简单例子
        //    @Query(value = "select name,author,price from Book b where b.price>?1 and b.price<?2")
        //    List<Book> findByPriceRange(long price1, long price2);
        //
        //
        //2.  Like表达式
        //    @Query(value = "select name,author,price from Book b where b.name like %:name%")
        //    List<Book> findByNameMatch(@Param("name") String name);
        //
        //3. 使用Native SQL Query
        //    所谓本地查询，就是使用原生的sql语句（根据数据库的不同，在sql的语法或结构方面可能有所区别）进行查询数据库的操作。
        //    @Query(value = "select * from book b where b.name=?1", nativeQuery = true)
        //    List<Book> findByName(String name);
        //
        //4. 使用@Param注解注入参数
        //    @Query(value = "select name,author,price from Book b where b.name = :name AND b.author=:author AND b.price=:price")
        //    List<Book> findByNamedParam(@Param("name") String name, @Param("author") String author,
        //                                @Param("price") long price);



    @Query(value = "SELECT * FROM user WHERE username=?", nativeQuery = true)
    public User findName(String name);

    @Query(value = "SELECT * FROM user WHERE id=:id", nativeQuery = true)
    public User findById(@PathVariable("id") int id);

}
