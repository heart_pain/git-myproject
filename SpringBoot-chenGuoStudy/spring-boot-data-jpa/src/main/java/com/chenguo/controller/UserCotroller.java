package com.chenguo.controller;

import com.chenguo.pojo.User;
import com.chenguo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserCotroller {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/getAll")
    public List<User> findAll(){
        List<User> all = userRepository.findAll();

        all.forEach(System.out::println);

        return all;
    }

    /*@RequestParam("username") String username,  请求 http://localhost:8080/getUserByName?username=chenguo 标识？ 后面的参数
    @PathVariable("username") String username 请求 http://localhost:8080/getUserByName/chenguo
    @RequestParam("password") String password,*/


    @RequestMapping("/getOne/{id}")
    public User findByOne(@PathVariable("id") int id){
        User all = userRepository.findById(id);

        //all.forEach(System.out::println);

        return all;
    }

    @RequestMapping("/getOne")
    public User findByOne2(@RequestParam("id") int id){
        User all = userRepository.findById(id);

        //all.forEach(System.out::println);

        return all;
    }




}
