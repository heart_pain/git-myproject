package com.chenguo.pojo;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by Donghua.Chen on 2018/7/24.
 */
public class Book {

    //空检查
    //@Null       验证对象是否为null
    //@NotNull    验证对象是否不为null, 无法查检长度为0的字符串
    //@NotBlank   检查约束字符串是不是Null还有被Trim的长度是否大于0,只对字符串,且会去掉前后空格.
    //@NotEmpty   检查约束元素是否为NULL或者是EMPTY.
    //
    //        Booelan检查
    //@AssertTrue     验证 Boolean 对象是否为 true
    //@AssertFalse    验证 Boolean 对象是否为 false
    //
    //长度检查
    //@Size(min=, max=) 验证对象（Array,Collection,Map,String）长度是否在给定的范围之内
    //@Length(min=, max=) string is between min and max included.
    //
    //        日期检查
    //@Past       验证 Date 和 Calendar 对象是否在当前时间之前
    //@Future     验证 Date 和 Calendar 对象是否在当前时间之后
    //@Pattern    验证 String 对象是否符合正则表达式的规则

    private Integer id;
    @NotBlank(message = "name 不允许为空")
    @Length(min = 2, max = 10, message = "name 长度必须在 {min} - {max} 之间")
    private String name;
    @NotNull(message = "price 不允许为空")
    @DecimalMin(value = "0.1", message = "价格不能低于 {value}")
    @Max(value = 50,message = "不能大于50")
    private BigDecimal price;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
