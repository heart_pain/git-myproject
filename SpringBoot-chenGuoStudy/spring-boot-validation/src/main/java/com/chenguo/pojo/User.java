package com.chenguo.pojo;/**
 * @Author chengguo
 * @ClassName user
 * @Date 2021-07-16  09:25
 * @Version 1.0
 **/

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author chengguo
 * @ClassName user
 * @Date 2021-07-16  09:25
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class User {


    private String id;
    @Value("陈果")
    private String username;
    @Value("123456")
    private String password;
}
