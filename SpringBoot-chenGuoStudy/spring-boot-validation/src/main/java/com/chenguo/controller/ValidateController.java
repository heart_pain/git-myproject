package com.chenguo.controller;

import com.chenguo.pojo.Book;
import com.chenguo.pojo.Person;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 *  后端验证、校验
 * Created by Chen on 2018/7/25.
 */
@Validated
@RestController
public class ValidateController {
    /*Contrller的类上添加@Validated,方法的形参的实体类上添加@Valid注解，
    后面紧跟着BindingResult result(必须强制性的)，校验的结果放在了result中。*/


    @GetMapping("/test2")
    public String test2(@NotBlank(message = "name 不能为空")
                            @Length(min = 2, max = 10, message = "name 长度必须在 {min} - {max} 之间") String name) {
        Person person = new Person();
        person.setName(name);
        return name;
    }

    //直接加到pojo类里面
    @GetMapping("/test3")
    public String test3(@Validated Book book) {
        return "success";
    }



}
