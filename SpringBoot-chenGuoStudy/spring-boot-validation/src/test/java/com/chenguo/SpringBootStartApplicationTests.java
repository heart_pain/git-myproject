package com.chenguo;

import com.chenguo.pojo.Person;
import com.chenguo.pojo.User;
import org.junit.jupiter.api.Test;

//import org.junit.jupiter.api.Test 用在Spring Boot 2.2.X以后
//import org.junit.Test用在2.2.x之前

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootStartApplicationTests {

    @Autowired
    private User user;

    @Autowired
    private Person person;

    @Test
    void contextLoads() {
        //可以通过@Value 注入值  但是一定要加 @Component
        System.out.println(user);
        //这里@ConfigurationProperties(prefix = "person") 指定配置文件  注入到person里面去的
        System.out.println(person);
    }


}
