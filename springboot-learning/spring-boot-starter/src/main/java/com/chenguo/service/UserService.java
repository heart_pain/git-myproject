package com.chenguo.service;


import com.chenguo.pojo.User;

import java.util.List;

public interface UserService {

    public List<User> getUser();

    public User loginUser(String username ,String password);
}
