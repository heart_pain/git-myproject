package com.chenguo.service.impl;

import com.chenguo.dao.Usermapper;
import com.chenguo.pojo.User;
import com.chenguo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public Usermapper usermapper;

    public List<User> getUser(){
        return usermapper.getAllUser();
    }

    @Override
    public User loginUser(String username, String password) {
        return usermapper.getUserByUsername(username,password);
    }
}
