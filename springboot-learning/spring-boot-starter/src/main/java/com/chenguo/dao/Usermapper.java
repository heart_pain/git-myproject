package com.chenguo.dao;

import com.chenguo.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface Usermapper {

    //获取所有用户
    public List<User> getAllUser();
    public User getUserByUsername(String name ,String password);
}
