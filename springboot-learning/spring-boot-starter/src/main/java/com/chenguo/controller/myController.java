package com.chenguo.controller;

import com.chenguo.pojo.User;
import com.chenguo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 我的第一个springboot程序
 * 其中 @RestController 等同于 （@Controller 与 @ResponseBody）
 */
@RestController
public class myController {

    @Autowired
    public UserService userService;

    @GetMapping("/hello")
    public  String helloWorld(){
        return "hello world";
    }


    @RequestMapping("/getUsers")
    public List<User> getUsers(){
       return userService.getUser();
    }
}
