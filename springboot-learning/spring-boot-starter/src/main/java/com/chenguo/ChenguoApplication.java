package com.chenguo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChenguoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChenguoApplication.class, args);
    }

}
