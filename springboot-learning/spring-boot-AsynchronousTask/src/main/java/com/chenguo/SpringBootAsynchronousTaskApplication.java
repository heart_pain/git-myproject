package com.chenguo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

//开启异步任务注解功能
@EnableAsync
@EnableScheduling //开启定时功能的注解
//@Scheduled //什么时候执行
@SpringBootApplication
public class SpringBootAsynchronousTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAsynchronousTaskApplication.class, args);
    }

}
