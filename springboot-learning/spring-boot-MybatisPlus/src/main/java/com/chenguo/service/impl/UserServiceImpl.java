package com.chenguo.service.impl;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chenguo.mapper.UserMapper;
import com.chenguo.pojo.User;
import com.chenguo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈果
 * @since 2021-07-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<BaseMapper<User> ,User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private BaseMapper<User> baseMapper;

    @Override
    public List<User> getUsers() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);

        List<User> users1 = baseMapper.selectList(null);

        users1.forEach(System.out::println);

        return users;
    }
}
