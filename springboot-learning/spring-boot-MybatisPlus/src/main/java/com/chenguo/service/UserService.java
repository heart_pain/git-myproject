package com.chenguo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.pojo.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 陈果
 * @since 2021-07-08
 */
public interface UserService extends IService<User>{

    public List<User> getUsers();


}
