package com.chenguo.common;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;


import java.util.ArrayList;

public class CodeGenerator {

        public static void main(String[] args) {
         /* // 需要构建一个 代码自动生成器 对象
             AutoGenerator mpg = new AutoGenerator();
            // 配置策略
            // 1、全局配置
            GlobalConfig gc = new GlobalConfig();
            String projectPath = System.getProperty("user.dir");

            gc.setFileOverride(false);// 是否覆盖同名文件，默认是false

            gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
            gc.setEnableCache(false);// XML 二级缓存
            gc.setBaseResultMap(true);// XML ResultMap
            gc.setBaseColumnList(false);// XML columList
            *//* 自定义文件命名，注意 %s 会自动填充表实体属性！ *//*

            gc.setServiceName("%sService");// 去Service的I前缀
            // gc.setMapperName("%sDao");
            // gc.setXmlName("%sDao");
            // gc.setServiceName("MP%sService");
            // gc.setServiceImplName("%sServiceDiy");
            // gc.setControllerName("%sAction");

            gc.setOutputDir(projectPath+"/spring-boot-MybatisPlus/src/main/java");
            gc.setAuthor("陈果"); gc.setOpen(false);

            gc.setIdType(IdType.ID_WORKER);
            gc.setDateType(DateType.ONLY_DATE);
            gc.setSwagger2(true);

            mpg.setGlobalConfig(gc);

            //2、设置数据源

            DataSourceConfig dsc = new DataSourceConfig();
            dsc.setUrl("jdbc:mysql://localhost:3306/mybatis_plus? useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8"); dsc.setDriverName("com.mysql.cj.jdbc.Driver");
            dsc.setUsername("root");
            dsc.setPassword("123456");
            dsc.setDbType(DbType.MYSQL);
            dsc.setDriverName("com.mysql.cj.jdbc.Driver");
            mpg.setDataSource(dsc);
            //3、包的配置
            PackageConfig pc = new PackageConfig();
            pc.setModuleName("blog");
            pc.setParent("com.chenguo");
            pc.setEntity("entity");
            pc.setMapper("mapper");
            pc.setService("service");
            pc.setController("controller");
            mpg.setPackageInfo(pc);


            //4、策略配置

            StrategyConfig strategy = new StrategyConfig();
            //("blog_tags","course","links","sys_settings","user_record"," user_say");  // 设置要映射的表名
            strategy.setInclude("user");// 需要生成的表

            // strategy.setCapitalMode(true);// 全局大写命名 ORACLE 注意
            //strategy.setTablePrefix(new String[] { "user_" });// 此处可以修改为您的表前缀

            strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
            strategy.setColumnNaming(NamingStrategy.underline_to_camel);
            strategy.setEntityLombokModel(true); // 自动lombok；

            //strategy.setLogicDeleteFieldName("deleted");//逻辑删除

            // strategy.setExclude(new String[]{"test"}); // 排除生成的表
            // 自定义实体父类
            // strategy.setSuperEntityClass("com.baomidou.demo.TestEntity");
            // 自定义实体，公共字段
            // strategy.setSuperEntityColumns(new String[] { "test_id", "age" });
            // 自定义 mapper 父类
            // strategy.setSuperMapperClass("com.baomidou.demo.TestMapper");
            // 自定义 service 父类
            // strategy.setSuperServiceClass("com.baomidou.demo.TestService");
            // 自定义 service 实现类父类
            // strategy.setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl");
            // 自定义 controller 父类
            // strategy.setSuperControllerClass("com.baomidou.demo.TestController");
            // 【实体】是否生成字段常量（默认 false）
            // public static final String ID = "test_id";
            // strategy.setEntityColumnConstant(true);
            // 【实体】是否为构建者模型（默认 false）
            // public User setName(String name) {this.name = name; return this;}
            // strategy.setEntityBuilderModel(true);

            // 自动填充配置
            //TableFill gmtCreate = new TableFill("create_time", FieldFill.INSERT);
            //TableFill gmtModified = new TableFill("update_time", FieldFill.INSERT_UPDATE);
            //ArrayList<TableFill> tableFills = new ArrayList<>();
            //tableFills.add(gmtCreate); tableFills.add(gmtModified);
            //strategy.setTableFillList(tableFills);
            // 乐观锁
            //strategy.setVersionFieldName("version"); 乐观锁
            //strategy.setRestControllerStyle(true);
            //strategy.setControllerMappingHyphenStyle(true);
            // localhost:8080/hello_id_2
            mpg.setStrategy(strategy);



                        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
            //        InjectionConfig cfg = new InjectionConfig() {
            //            @Override
            //            public void initMap() {
            //                Map<String, Object> map = new HashMap<String, Object>();
            //                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
            //                this.setMap(map);
            //            }
            //        };
            //
            //        // 自定义 xxList.jsp 生成
            //        List<FileOutConfig> focList = new ArrayList<>();
            //        focList.add(new FileOutConfig("/template/list.jsp.vm") {
            //            @Override
            //            public String outputFile(TableInfo tableInfo) {
            //                // 自定义输入文件名称
            //                return "D://my_" + tableInfo.getEntityName() + ".jsp";
            //            }
            //        });
            //        cfg.setFileOutConfigList(focList);
            //        mpg.setCfg(cfg);
            //
            //        // 调整 xml 生成目录演示
            //        focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
            //            @Override
            //            public String outputFile(TableInfo tableInfo) {
            //                return "/develop/code/xml/" + tableInfo.getEntityName() + ".xml";
            //            }
            //        });
            //        cfg.setFileOutConfigList(focList);
            //        mpg.setCfg(cfg);
            //
            //        // 关闭默认 xml 生成，调整生成 至 根目录
            //        TemplateConfig tc = new TemplateConfig();
            //        tc.setXml(null);
            //        mpg.setTemplate(tc);

            // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
            // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
            // TemplateConfig tc = new TemplateConfig();
            // tc.setController("...");
            // tc.setEntity("...");
            // tc.setMapper("...");
            // tc.setXml("...");
            // tc.setService("...");
            // tc.setServiceImpl("...");
            // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
            // mpg.setTemplate(tc);

            // 打印注入设置【可无】
            //        System.err.println(mpg.getCfg().getMap().get("abc"));
            mpg.execute(); //执行
            */

        }
    }
