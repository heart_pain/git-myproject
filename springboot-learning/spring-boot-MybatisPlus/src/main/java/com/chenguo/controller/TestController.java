package com.chenguo.controller;


import com.chenguo.pojo.User;
import com.chenguo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpCookie;
import java.util.*;

@Controller
public class TestController {


    @Autowired
    UserService  userService;

    @RequestMapping("/getUser")
    @ResponseBody
    public String test1(Model model , HttpServletRequest request , HttpServletResponse response, HttpSession session) throws IOException {
        List<User> users = userService.getUsers();
        //
        request.setAttribute("user2",users);
        Cookie cookie = new Cookie("user3","ABC");
        response.addCookie(cookie);
        session.setAttribute("user4",users);
        model.addAttribute("user1",users);
        //request.setAttribute();
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html; charset=utf-8");
       /* PrintWriter writer = response.getWriter();
        Map<String, Object> map = new HashMap<>();
        map.put("user", users);
        writer.write(map.toString());*/
        return "查询成功" +
                "";
    }





}
