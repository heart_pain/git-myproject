package com.chenguo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chenguo.pojo.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author cg
 * @Date 19-7-26 下午5:01
 * @Description TODO
 **/

public interface UserService  extends IService<User> {

}
