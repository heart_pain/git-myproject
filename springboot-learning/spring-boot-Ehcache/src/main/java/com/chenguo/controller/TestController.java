package com.chenguo.controller;


import com.chenguo.pojo.User;
import com.chenguo.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.util.List;

@Controller
@Slf4j
public class TestController {

    private final Logger logger = LoggerFactory.getLogger( TestController.class );

    @Autowired
    UserService userService;


    @GetMapping("/queryAll")
    @ApiOperation(value = "查询所有用户信息")
    @ResponseBody
    public List<User> test(){
        List<User> list = userService.list(null);
        list.forEach(System.out::println);
        return userService.list(null);
    }

    @GetMapping("/query/{id}")
    @ApiOperation("查询某一个用户")
    public User queryById(@RequestParam Long id){
        return userService.getById(id);
    }

    @PostMapping("/save")
    @ApiOperation("保存用户信息")
    public Boolean save(@RequestBody User user){
        return userService.save(user);
    }

    @PostMapping("/update")
    @ApiOperation("更新用户信息")
    public Boolean update(@RequestBody User user) throws Exception {
        boolean b = userService.updateById(user);
        return b;
    }



}
