package com.chenguo.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author cg
 * @Date 19-7-26 下午5:00
 * @Description TODO
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String username;
    private Integer age;
    private String sex;

    // 对应数据库中的主键 (uuid、自增id、雪花算法、redis、zookeeper！)

    //@Version //乐观锁Version注解
    //private Integer version;
    //@TableLogic //逻辑删除
    //private Integer deleted;
    // 字段添加填充内容
    @TableField(fill = FieldFill.INSERT)
    private Date create_time;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date update_time;




}
