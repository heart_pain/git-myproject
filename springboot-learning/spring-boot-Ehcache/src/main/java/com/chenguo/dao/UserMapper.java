package com.chenguo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chenguo.pojo.User;


public interface UserMapper extends BaseMapper<User> {}
