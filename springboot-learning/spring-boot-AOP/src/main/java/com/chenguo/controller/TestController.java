package com.chenguo.controller;


import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

@Controller
@RequestMapping("/AOP")
public class TestController {



        @RequestMapping(value = "/demo",method = RequestMethod.GET)
        @ResponseBody
        public String test1(@RequestParam(value = "name",required = true)
                                        String name,
                                        HttpServletRequest request,
                                        HttpServletResponse response,
                                        Session session,
                                        Model model){
            model.addAttribute("msg","Hello,Springboot！！！");//
            request.setAttribute("msg1","你好");//request.setAttrbute 是绑定到哪儿
            //response.addCookie("msg2","你好");
            System.out.println("这是执行方法");
            //System.out.println(session.getCookie("msg2"));
            //classpath:/templates/test.html
            return "returnName";
        }





}
