package com.chenguo.JedisTest;

import com.alibaba.fastjson.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;


//Redis的事务
public class TestTX {
    public static void main(String[] args) {
        //连接redis
        Jedis jedis = new Jedis("127.0.0.1", 6379);

        //清空当前数据库
        jedis.flushDB();

        //通过fastjson获取json字符串
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hello","world");
        jsonObject.put("name","kuangshen");

        // 1.开启事务
        Transaction multi = jedis.multi();
        String result = jsonObject.toJSONString();

        //乐观锁（监控result）
        //jedis.watch(result);
        try {
            //2.命令
            multi.set("user1",result);
            multi.set("user2",result);

            // 代码抛出异常事务，执行失败！
            //int i = 1/0 ;

            // 3.执行事务！
            multi.exec();
        } catch (Exception e) {
            multi.discard(); // 失败就放弃事务
            e.printStackTrace();
        } finally {
            System.out.println(jedis.get("user1"));
            System.out.println(jedis.get("user2"));
            jedis.close(); // 关闭连接
        }
    }
}
