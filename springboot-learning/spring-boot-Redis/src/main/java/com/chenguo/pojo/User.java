package com.chenguo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@AllArgsConstructor  //有参
@NoArgsConstructor   //无参
@Data                //get、set、equals、hashCode、canEqual、toString
public class User implements Serializable {

    private String name;
    private int age;
}
