package com.chenguo;

import com.chenguo.pojo.User;
import com.chenguo.util.RedisUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class SpringBootRedisApplicationTests {


    //1.基本命令（原生命令，实际开发中需要使用工具类RedisUtils）
    // redisTemplate  操作不同的数据类型，api和我们的指令是一样的
    // opsForValue    操作字符串 类似String
    // opsForList     操作List 类似List
    // opsForSet      操作Set
    // opsForHash     操作Hash
    // opsForZSet     操作ZSet
    // opsForGeo      操作Geo
    // opsForHyperLogLog  操作HyperLogLog

    //2.简化的基本命令（自己封装工具类）
    // 除了基本的操作，我们常用的方法都可以直接通过redisTemplate操作，比如事务，和基本的CRUD

    // 3.获取redis数据库的连接对象
    //        RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
    //        connection.flushDb();
    //        connection.flushAll();

    @Autowired
    @Qualifier("redisTemplate") //指定自定义的redisTemplate（不用自带的redisTemplate）
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    //测试使用工具类
    @Test
    public void test1(){
        redisUtil.set("name","kuangshen");
        System.out.println(redisUtil.get("name"));
    }

    //原生命令（不用工具类）
    @Test
    void contextLoads() {
        redisTemplate.opsForValue().set("mykey","关注狂神说公众号");
        System.out.println(redisTemplate.opsForValue().get("mykey"));
    }

    //序列化（使用自己配置的RedisTemplate）
    @Test
    public void test() throws JsonProcessingException {
        User user=new User("kuansgehn",2);

        //将传入的对象序列化为json（自己配置RedisTemplate后就不需要这个）
        //String jsonUser=new ObjectMapper().writeValueAsString(user);

        redisTemplate.opsForValue().set("user", user);
        System.out.println(redisTemplate.opsForValue().get("user"));
    }

}
