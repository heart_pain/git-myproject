package com.chenguo.dao;

import com.chenguo.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Author: huahua
 * @Date: 2021-04-20 12:52
 */
@Repository
@Mapper
public interface UserMapper {
    User login(@Param("name") String name, @Param("password") String password);

    User findUserByName(@Param("name") String name);
}
